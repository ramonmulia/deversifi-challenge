# DeversiFi Development Challenge

### Installation
This project requires Node.js v12.13.0 to run.

Install the dependencies and start the server.
```sh
cd deversifi-challenge
npm i
npm run dev
```

### Configutation
You can set configurations on config/default.json