const config = require('config');
const WS = require('./lib/ws');
const User = require('./model/user');
const Robot = require('./lib/robot');


const run = () => {
    const user = new User(config.USER_BUDGET);
    const ws = new WS(config.API_URI);
    const robot = new Robot(ws, user, config.PAIR, config.CANDLE_TIME_FRAME);
    
    robot.starts();
}

run();
