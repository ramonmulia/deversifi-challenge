const cleanupProcess = (cleanup) => {
  process.on("cleanup", () => cleanup());
  process.on("exit", () => cleanup());
  process.on("SIGINT", () => cleanup());
  process.on("uncaughtException", () => cleanup());
};

module.exports = cleanupProcess;
