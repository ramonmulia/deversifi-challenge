const first = require('lodash/first');
const cleanup = require("./cleanup");
const Ticker = require("../model/ticker");
const Candles = require("../model/candles");

class Robot {
  constructor(ws, user, pair, candleTimeFrame) {
    this.ws = ws;
    this.user = user;
    this.pair = pair;
    this.candleTimeFrame = candleTimeFrame;
    this.ticker = {};
    this.candles = [];
    this.lastOrder = 0;
    this.lastAmount = 0;

    this.ws.on("open", this.onOpen);
    this.ws.on("ticker", this.handleTicker);
    this.ws.on("candles", this.handleCandle);
  }

  starts = () => {
    this.ws.open();
  };

  onOpen = () => {
    this.ws.subscribePair(this.pair, this.candleTimeFrame);
    cleanup(this.ws.unsubscribe);
  };

  handleTicker = json => {
    this.ticker = new Ticker(json[1]);
    this.execAlgorithm();
  };

  handleCandle = json => {
    this.candles = new Candles(json[1]);
    this.execAlgorithm();
  };

  execAlgorithm = () => {
    const { bid, ask, lastPrice } = this.ticker;
    const tickerLow = this.ticker.low;
    const candle = first(this.candles.list) || {};
    const lowCandle = candle.low;
    const closeCandle = candle.close;

    if(lowCandle <= bid && lastPrice <= closeCandle && this.user.budget > 0){
      const price = (lowCandle + bid)/2;
      const isAllowed = this.user.buy(price, lastPrice);
      if(isAllowed){
        this.lastOrder = price;
      }
      else{
        const price = this.user.budget;
        this.user.buy(price, lastPrice);
        this.lastOrder = price;
      }
      this.print('buy');
    }
    else if(ask > tickerLow && ask > lowCandle && this.user.coinAmount > 0){
      const price = (ask + bid)/2;
      const isAllowed = this.user.sell(price, lastPrice);
      if(isAllowed){
        this.lastOrder = price;
        this.print('sell');
      }
    }
  };

  print = operation =>
    console.log(`operation: ${operation.toUpperCase()}\tprice: ${this.lastOrder}\tamount: ${this.user.coinAmount}\tpair: ${this.pair}\tuser budget: $${this.user.budget}`);
}

module.exports = Robot;
