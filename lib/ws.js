const { EventEmitter } = require("events");
const WebSocket = require("ws");
const get = require("lodash/get");
const first = require("lodash/first");
const keys = require("lodash/keys");

class WS extends EventEmitter {
  constructor(API_URI) {
    super();
    this.API_URI = API_URI;
    this.channelInfo = {};
  }

  open() {
    this.ws = new WebSocket(this.API_URI);
    this.ws.on("message", this.onMessage);
    this.ws.on("open", this.onOpen);
    this.ws.on("error", this.onError);
    this.ws.on("close", this.onClose);
  }

  setChannel = ({ chanId, channel }) => (this.channelInfo[chanId] = channel);

  onMessage = (msgJson, flags) => {
    try {
      const msg = JSON.parse(msgJson);

      if (get(msg, "event") === "subscribed") {
        this.setChannel(msg);
        return;
      }

      const channelEvent = this.channelInfo[first(msg)];
      this.emit(channelEvent, msg, flags);
    } catch (e) {
      console.error(`Error: ${e.message}`);
    }
  };

  onOpen = () => this.emit("open");

  onError = error => this.emit("error", error);

  onClose = () => this.emit("close");

  send = jsonObj => {
    const msg = JSON.stringify(jsonObj);
    this.ws.send(msg);
  };

  subscribePair = (pair, candleTimeFrame) => {
    this.send({
      event: "subscribe",
      channel: "ticker",
      symbol: pair
    });

    this.send({
      event: "subscribe",
      channel: "candles",
      key: `trade:${candleTimeFrame}:${pair}`
    });
  };

  unsubscribe = () => {
    keys(this.channelInfo).forEach(chanId =>
      this.send({
        event: "unsubscribe",
        chanId
      })
    );
  };
}

module.exports = WS;
