const isArray = require("lodash/isArray");
const first = require("lodash/first");

class Candle {
  candleProperties = ["mts", "open", "close", "high", "low", "volume"];
  constructor(candleArray) {
    this.candleProperties.reduce((prev, current, index) => {
      prev[current] = candleArray[index];
      return prev;
    }, this);
  }
}

class Candles {
  constructor(candlesArray) {
    this.list = [];
    if (isArray(candlesArray)) {
      if (isArray(first(candlesArray))) {
        this.list = candlesArray.map(candle => new Candle(candle));
      } else {
        this.list = [new Candle(candlesArray)];
      }
    }
  }
}

module.exports = Candles;
