const isArray = require("lodash/isArray");

class Ticker {
  tickerProperties = [
    "bid",
    "bidSize",
    "ask",
    "askSize",
    "dailyChange",
    "dailyChangeRelative",
    "lastPrice",
    "volume",
    "high",
    "low"
  ];

  constructor(ticker) {
    if (isArray(ticker)) {
      this.convertArrayToTickerObj(ticker);
    }
  }

  convertArrayToTickerObj = (tickerArray) => {
    this.tickerProperties.reduce((prev, current, index) => {
        prev[current] = tickerArray[index];
        return prev;
      }, this);
  }
}

module.exports = Ticker;
