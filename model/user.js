class User {
  constructor(budget) {
    this.budget = budget;
    this.coinAmount = 0;
  }

  hasBudget = orderPrice => !(orderPrice > this.budget);

  buy = (orderPrice, currentPrice) => {
    if (this.hasBudget(orderPrice)) {
      this.budget -= orderPrice;
      this.coinAmount += orderPrice / currentPrice;
      return true;
    }
    return false;
  };

  sell = (orderPrice, currentPrice) => {
    if (this.coinAmount > 0) {
      const coinAmountCalculated = orderPrice / currentPrice;
      if(coinAmountCalculated <= this.coinAmount){
        this.coinAmount -= coinAmountCalculated;
        this.budget += orderPrice;
        return true;
      }
    }

    return false;
  };
}

module.exports = User;
